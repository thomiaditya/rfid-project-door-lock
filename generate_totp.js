const fs = require('fs');
const speakeasy = require('speakeasy');
const QRCode = require('qrcode');

// Define the directory where you want to save the files
const directory = './totp_secrets';

// Generate a TOTP secret
const secret = speakeasy.generateSecret();

// Create the directory if it does not exist
if (!fs.existsSync(directory)){
    fs.mkdirSync(directory);
}

// Save the secret to a file within the directory
fs.writeFileSync(`${directory}/secret.json`, JSON.stringify(secret), (err) => {
  if (err) throw err;
  console.log('Secret saved to secret.json.');
});

// Generate a QR code and save it as 'qrcode.png' within the directory
QRCode.toFile(`${directory}/qrcode.png`, secret.otpauth_url, (err) => {
  if (err) throw err;
  console.log('QR Code saved to qrcode.png.');
});
