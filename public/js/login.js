document.addEventListener("DOMContentLoaded", function () {
    let alertCopy;
    const alertContainer = document.querySelector(
        "[role=alert-message-container]"
    );
    const form = document.getElementById("loginForm"); // Replace with your form's ID
    const submitLogin = document.querySelector("[role='submit-login']");

    (function init() {
        const alert = document.querySelector("[role=alert-message]");
        alertCopy = alert.cloneNode(true);
        alertCopy.classList.remove("hidden");

        removeAlert();
        form.addEventListener("submit", handleSubmitForm);
    })();

    function removeAlert() {
        const alert = document.querySelector("[role=alert-message]");
        if (!alert) return;
        alert.remove();
    }

    function showAlert() {
        alertContainer.appendChild(alertCopy);
    }

    function handleSubmitForm(event) {
        event.preventDefault(); // Prevent the default form submission

        const formData = new FormData(form);
        removeAlert();

        submitLogin.setAttribute("disabled", true);

        setTimeout(function () {
            fetch(form.action, {
                method: "POST",
                body: JSON.stringify(Object.fromEntries(formData.entries())),
                headers: {
                    "Content-Type": "application/json",
                },
            })
                .then((response) => {
                    if (!response.ok) {
                        throw new Error("HTTP status " + response.status);
                    }
                    return response.json();
                }) // Assuming the server responds with JSON
                .then((data) => {
                    console.log("Success:", data);
                    window.location.replace("/admin/home");
                })
                .catch((error) => {
                    console.error("Error:", error);
                    showAlert();
                    submitLogin.removeAttribute("disabled");
                });
        }, 1000);
    }
});
