document.addEventListener("DOMContentLoaded", function () {
    const userTableContainer = document.getElementById("user-table-container");
    const rfidTableContainer = document.getElementById("rfid-table-container");
    const historyTableContainer = document.getElementById("history-table-container");
    const rfidCreateUserSelect = document.getElementById("rfid-create-user");
    const saveButtonCreateUser = document.querySelector(
        "[data-role=save-button-create-user]"
    );
    const formCreateUser = document.getElementById("form-create-user");
    let rfids;

    let phoneInput = "";

    init();
    function init() {
        rfids = fetchRFIDs();
        fetchDataAndPlace("/api/rfid", rfidTableContainer, addRFIDsToDOM);
        fetchDataAndPlace("/api/user", userTableContainer, addUsersToDOM);
        fetchDataAndPlace("/api/rfid/historyLogs", historyTableContainer, addHistoryToDOM);

        addRFIDToAddUserModal();
        document
            .getElementById("phone")
            .addEventListener("keydown", handleInputPhone);

        saveButtonCreateUser.addEventListener("click", function (e) {
            this.setAttribute("disabled", true);

            let phoneData = formCreateUser
                .querySelector("#phone")
                .getAttribute("data-phone");
            const formData = new FormData(formCreateUser);

            formData.set("phone", phoneData);

            fetch("/api/user/create", {
                method: "POST",
                body: JSON.stringify(Object.fromEntries(formData.entries())),
                headers: {
                    "Content-Type": "application/json",
                },
            })
                .then((response) => {
                    if (!response.ok) {
                        throw new Error("HTTP status " + response.status);
                    }
                    return response.json();
                }) // Assuming the server responds with JSON
                .then((data) => {
                    // console.log("Success:", data);
                    location.reload();
                })
                .catch((error) => {
                    console.error("Error:", error);
                    alert("Error adding user!");
                    this.removeAttribute("disabled");
                });
        });
    }

    function addRFIDToAddUserModal() {
        function optionString(rfid, selected = true) {
            if (selected) {
                return `<option value="${rfid.id}" selected="">${rfid.tag_id}</option>`;
            }
            return `<option value="${rfid.id}">${rfid.tag_id}</option>`;
        }

        let options = "";
        rfids.data.forEach((rfid) => {
            options += optionString(rfid, false);
        });

        options =
            `<option value="null" selected="">Select RFID</option>` + options;

        rfidCreateUserSelect.innerHTML = "";
        rfidCreateUserSelect.insertAdjacentHTML("beforeend", options);
    }

    function addHistoryToDOM(listItem, tbody) {
        listItem.forEach((item) => {
            tbody.insertAdjacentHTML("beforeend", historyTemplate(item));
        });
    }

    function handleInputPhone(event) {
        // Allow backspace, delete, arrow keys, tab, and space
        var allowedKeys = [
            "Backspace",
            "Delete",
            "ArrowLeft",
            "ArrowRight",
            "ArrowUp",
            "ArrowDown",
            "Tab",
        ];

        // Check if the pressed key is allowed
        if (allowedKeys.includes(event.key)) {
            return true; // Allow default action
        }

        event.preventDefault();
        const content = !event.target.value
            ? event.key
            : event.target.value + event.key;
        phoneInput = content.replace(/\D/g, "");
        event.target.setAttribute("data-phone", phoneInput);
        let group = phoneInput.match(/^(0|62)(\d{0,3})(\d{0,4})(\d{0,4})/);
        if (!group) {
            return;
        }
        event.target.value =
            "+62 " +
            (group[2].length == 3 ? group[2] + "-" : group[2]) +
            (group[3].length == 4 ? group[3] + "-" : group[3]) +
            group[4];
    }

    function fetchRFIDs() {
        const request = new XMLHttpRequest();
        request.open("GET", "/api/rfid", false); // `false` makes the request synchronous
        request.send(null);

        if (request.status === 200) {
            const jsonRes = JSON.parse(request.responseText);
            // console.log(jsonRes);
            return jsonRes;
        }
        return null;
    }

    function fetchDataAndPlace(path, tableContainer, fnOnSuccess) {
        const tbody = tableContainer.querySelector("tbody");
        const loading = tableContainer.querySelector("[data-loading]");
        tbody.innerHTML = "";
        loading.classList.remove("hidden");
        fetch(path, {
            method: "GET",
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error("HTTP status " + response.status);
                }
                return response.json();
            })
            .then((data) => {
                const listItem = data.data;
                // console.log("Success:", listItem);
                tbody.innerHTML = "";

                fnOnSuccess(listItem, tbody);

                loading.classList.add("hidden");
            })
            .catch((error) => {
                console.error("Error:", error);
            });
    }

    function addUsersToDOM(listItem, tbody) {
        listItem.forEach((item) => {
            tbody.insertAdjacentHTML("beforeend", userTemplate(item));
            const selectEl = tbody.querySelector(
                `#select-rfid-user-${item.id}`
            );

            selectEl.addEventListener("change", (e) => {
                e.preventDefault();
                const changedId = e.target.value;
                editUser(item.id, { rfid_id: changedId });
            });
        });
    }

    function editUser(id, data) {
        fetch(`/api/user/${id}/edit`, {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error("HTTP status " + response.status);
                }
                return response.json();
            }) // Assuming the server responds with JSON
            .then((data) => {
                // console.log("Success:", data);
                init();
            })
            .catch((error) => {
                console.error("Error:", error);
                alert("Error changing RFID");
                init();
                // location.reload();
            });
    }

    function addRFIDsToDOM(listItem, tbody) {
        listItem.forEach((item) => {
            tbody.insertAdjacentHTML("beforeend", rfidTemplate(item));
        });
    }

    function userTemplate(data) {
        function optionString(rfid, selected = true) {
            if (selected) {
                return `<option value="${rfid.id}" selected="">${rfid.tag_id}</option>`;
            }
            return `<option value="${rfid.id}">${rfid.tag_id}</option>`;
        }

        let options = "";

        rfids.data.forEach((rfid) => {
            if (rfid.id == data.rfid_id) {
                options += optionString(rfid, true);
                return;
            }

            options += optionString(rfid, false);
        });

        if (data.rfid_id == null) {
            options = optionString({ id: null, tag_id: null }, true) + options;
        } else {
            options = optionString({ id: null, tag_id: null }, false) + options;
        }

        return `
        <tr class="${data.rfid?.checked_in ? "bg-green-50" : ""}">
            <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-800">
                ${data.name}</td>
            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-800">${data.email}</td>
            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-800">${data.phone}</td>
            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-800"><select
                        class="select-rfid" name="select-rfid" id="select-rfid-user-${data.id}">
                        ${options}
                    </select></td>
            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-800">${data.status.name}</td>
            <td class="px-6 py-4 whitespace-nowrap text-end text-sm font-medium flex gap-2">
                
                <button type="button" data-role="delete-button"
                    class="inline-flex items-center gap-x-2 text-sm font-semibold rounded-lg border border-transparent text-red-600 hover:text-red-800 disabled:opacity-50 disabled:pointer-events-none" onclick="deleteUser(${data.id})">Delete</button>
            </td>
        </tr>
        `;
    }

    function rfidTemplate(data) {
        const date = new Date(data.createdAt);
        const formattedDate = date.toLocaleString("id-ID");

        return `
        <tr class="${data.checked_in ? "bg-green-50" : ""}">
            <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-800">
                ${data.tag_id}</td>
            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-800">${
                data.last_seen
                    ? new Date(data.last_seen).toLocaleString("id-ID")
                    : "never"
            }</td>
            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-800">${
                data.checked_in ? "in room" : "not in room"}</td>
            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-800">${formattedDate}</td>
            <td class="px-6 py-4 whitespace-nowrap text-end text-sm font-medium">
                <button type="button"
                    class="inline-flex items-center gap-x-2 text-sm font-semibold rounded-lg border border-transparent text-red-600 hover:text-red-800 disabled:opacity-50 disabled:pointer-events-none" onclick="deleteRFID('${data.rfid_uid}')">Delete</button>
            </td>
        </tr>
        `;
    }

    function historyTemplate(data) {
        const date = new Date(data.createdAt);
        const formattedDate = date.toLocaleString("id-ID");

        return `
        <tr class="${data.is_checked_in ? "bg-green-50" : "bg-red-50"}">
            <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-800">
                ${data.user?.name ? data.user.name : "null"}</td>
            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-800">${data.user?.email ? data.user.email : "null"}</td>
            ${data.is_checked_in ? `<td class="px-6 py-4 whitespace-nowrap text-sm text-green-800">Masuk</td>` : `<td class="px-6 py-4 whitespace-nowrap text-sm text-red-800">Keluar</td>`}
            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-800">${data.rfid.tag_id}</td>
            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-800 text-end">${formattedDate}</td>
        </tr>
        `;
    }
});

function deleteUser(id) {
    const confirmation = confirm("Are you sure you want to delete this user?");

    if (!confirmation) {
        return;
    }

    fetch(`/api/user/${id}/delete`, {
        method: "POST",
    })
        .then((response) => {
            if (!response.ok) {
                throw new Error("HTTP status " + response.status);
            }
            return response.json();
        }) // Assuming the server responds with JSON
        .then((data) => {
            // console.log("Success:", data);
            window.location.reload();
        })
        .catch((error) => {
            console.error("Error:", error);
            alert("Error deleting user");
            window.location.reload();
        });
}

function deleteRFID(id) {
    const confirmation = confirm("Are you sure you want to delete this RFID?");
    if (!confirmation) {
        return;
    }    

    fetch(`/api/rfid/delete`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({ rfid_uid: id }),
    })
        .then((response) => {
            if (!response.ok) {
                throw new Error("HTTP status " + response.status);
            }
            return response.json();
        }) // Assuming the server responds with JSON
        .then((data) => {
            // console.log("Success:", data);
            // Reload the page
            window.location.reload();
        })
        .catch((error) => {
            console.error("Error:", error);
            alert("Error deleting RFID");
            window.location.reload();
        });
}

function logout() {
    const confirmation = confirm("Are you sure you want to log out?");
    if (!confirmation) {
        return;
    }

    fetch(`/api/admin/logout`, {
        method: "POST",
    })
        .then((response) => {
            if (!response.ok) {
                throw new Error("HTTP status " + response.status);
            }
            return response.json();
        }) // Assuming the server responds with JSON
        .then((data) => {
            alert("Logged out successfully");
            // Reload the page
            window.location.reload();
        })
        .catch((error) => {
            console.error("Error:", error);
            alert("Error logging out");
            window.location.reload();
        });
}