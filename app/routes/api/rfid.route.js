const express = require("express");
const { body } = require("express-validator");
const { requireAuth } = require("../../middleware/session.middleware");
const rfidController = require("../../controllers/rfid.controller");

const router = express.Router();

router.get("/", requireAuth, rfidController.listRFIDs);
router.post(
    "/create",
    requireAuth,
    [
        body("rfid_uid").notEmpty().withMessage("RFID UID is required"),
        body("tag_id").notEmpty().withMessage("Tag ID is required"),
    ],
    rfidController.createRFID
);
router.post(
    "/validate",
    [body("rfid_uid").notEmpty().withMessage("RFID UID is required")],
    rfidController.validate
);

router.post(
    "/checkin/confirm",
    [body("rfid_uid").notEmpty().withMessage("RFID UID is required")],
    rfidController.checkInConfirm
);

router.post(
    "/checkout",
    [body("rfid_uid").notEmpty().withMessage("RFID UID is required")],
    rfidController.checkOut
);

router.post(
    "/checkout/confirm",
    [body("rfid_uid").notEmpty().withMessage("RFID UID is required")],
    rfidController.checkOutConfirm
);

router.post(
    "/delete",
    requireAuth,
    [
        body("rfid_uid").notEmpty().withMessage("RFID UID is required"),
    ],
    rfidController.deleteRFID
);

router.get(
    "/historyLogs",
    requireAuth,
    rfidController.listHistoryLogs
)

module.exports = router;
