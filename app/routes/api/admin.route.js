const { requireAuth } = require("../../middleware/session.middleware")

const express = require("express");
const { body } = require("express-validator");
const adminController = require('../../controllers/admin.controller')

const router = express.Router();

const validateAdminLogin = [
    body("email").isEmail().withMessage("Invalid email"),
    body("password")
        .isLength({ min: 6 })
        .withMessage("Password must be at least 6 characters long"),
];

router.post("/login", validateAdminLogin, adminController.adminLogin);
router.post("/logout", requireAuth, adminController.adminLogout);

module.exports = router;
