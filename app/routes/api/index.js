const express = require("express");
const userRoutes = require("./user.route");
const adminRoutes = require("./admin.route");
const rfidRoutes = require("./rfid.route");

const router = express.Router();

router.use('/user', userRoutes);
router.use('/admin', adminRoutes);
router.use("/rfid", rfidRoutes);

// Add other routes here, e.g.:
// router.use('/products', productRoutes);
// router.use('/orders', orderRoutes);

module.exports = router;
