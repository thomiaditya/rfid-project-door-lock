const { requireAuth } = require("../../middleware/session.middleware");

const express = require("express");
const userController = require("../../controllers/user.controller");
const { body } = require("express-validator");

const router = express.Router();

router.get("/", requireAuth, userController.listUsers);
router.post(
    "/create",
    requireAuth,
    [
        body("name").notEmpty().withMessage("Name is required"),
        body("email").isEmail().withMessage("Invalid email"),
        body("phone").optional(),
        body("rfid_id").optional(),
        // Add more validation rules as needed
    ],
    userController.createUser
);

router.post(
    "/:id/edit",
    [
        body("name").optional(),
        body("email").isEmail().withMessage("Invalid email").optional(),
        body("phone").optional(),
        body("rfid_id").optional(),
    ],
    requireAuth,
    userController.updateUser
);

router.post(
    "/:id/delete",
    requireAuth,
    userController.deleteUser
)

module.exports = router;
