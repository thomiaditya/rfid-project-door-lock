const {
    requireAuth,
    requireAuthWeb,
} = require("../../middleware/session.middleware");

const express = require("express");
const utils = require("../../utils");
const adminController = require("../../controllers/admin.controller");

const router = express.Router();

router.get("/login/:token", (req, res) => {
    const userToken = req.params.token;

    const verified = utils.verifyOTP(userToken);
    // TODO: Uncomment this for production
    // if (!verified) {
    //     return res.status(404).json({ success: false, error: "404 Not Found!" });
    // }

    if (req.session.adminId) {
        return res.redirect("/admin/home");
    }

    return res.render("login");
});

router.get("/admin/home", requireAuthWeb, (req, res) => {
    res.render("admin/home")
});

module.exports = router;
