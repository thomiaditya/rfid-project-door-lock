const logger = require('../logger');

const requestLogger = (req, res, next) => {
  const { method, url, headers, body } = req;
  const logMessage = `${method} ${url}`;

  logger.notice(logMessage);

  next();
};

module.exports = requestLogger;