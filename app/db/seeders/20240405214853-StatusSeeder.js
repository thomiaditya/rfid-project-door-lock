"use strict";

const moment = require('moment-timezone');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        await queryInterface.bulkInsert(
            "statuses",
            [
                {
                    name: "ACTIVE",
                    createdAt: moment().format(),
                    updatedAt: moment().format(),
                },
                {
                    name: "BLOCKED",
                    createdAt: moment().format(),
                    updatedAt: moment().format(),
                },
            ],
            {}
        );
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        await queryInterface.bulkDelete("statuses", null, {});
    },
};
