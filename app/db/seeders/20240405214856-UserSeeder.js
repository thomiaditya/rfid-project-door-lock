"use strict";

const moment = require('moment-timezone');
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        const statusActive = await queryInterface.rawSelect(
            "statuses",
            {
                where: { name: "ACTIVE" },
            },
            ["id"]
        );

        await queryInterface.bulkInsert(
            "users",
            [
                {
                    name: "John Doe",
                    email: "john.doe@example.com",
                    phone: "123456789",
                    status_id: statusActive,
                    createdAt: moment().format(),
                    updatedAt: moment().format(),
                },
                {
                    name: "Jane Smith",
                    email: "jane.smith@example.com",
                    phone: "987654321",
                    status_id: statusActive,
                    createdAt: moment().format(),
                    updatedAt: moment().format(),
                },
            ],
            {}
        );
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        await queryInterface.bulkDelete("users", null, {});
    },
};
