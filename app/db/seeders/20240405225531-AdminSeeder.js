"use strict";

const bcrypt = require("bcrypt");
const moment = require('moment-timezone');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        // Insert initial admin data
        // Hash passwords before inserting
        const hashedPassword = await bcrypt.hash("admin123", 10);

        await queryInterface.bulkInsert(
            "admins",
            [
                {
                    email: "admin@example.com",
                    password: hashedPassword,
                    createdAt: moment().format(),
                    updatedAt: moment().format(),
                },
                // Add more admin data if needed
            ],
            {}
        );
    },

    down: async (queryInterface, Sequelize) => {
        // Remove inserted admin data
        await queryInterface.bulkDelete("admins", null, {});
    },
};
