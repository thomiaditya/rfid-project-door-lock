"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Log extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            Log.belongsTo(models.User, {
                foreignKey: "user_id",
                as: "user",
            });
            Log.belongsTo(models.RFIDTag, {
                foreignKey: "rfid_id",
                as: "rfid",
            });
        }
    }
    Log.init(
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            rfid_id: {
                type: DataTypes.INTEGER,
            },
            user_id: {
                type: DataTypes.INTEGER,
            },
            is_checked_in: {
                type: DataTypes.BOOLEAN,
            },
        },
        {
            sequelize,
            modelName: "Log",
        }
    );
    return Log;
};
