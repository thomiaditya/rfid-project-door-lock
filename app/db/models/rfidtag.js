"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class RFIDTag extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            RFIDTag.hasOne(models.User, {
                foreignKey: "rfid_id",
                as: "user"
            });
        }
    }
    RFIDTag.init(
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            rfid_uid: {
                type: DataTypes.STRING,
                unique: true,
                allowNull: false,
            },
            tag_id: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            last_seen: {
                type: DataTypes.DATE,
                allowNull: true,
            },
            checked_in: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            }
        },
        {
            sequelize,
            modelName: "RFIDTag",
        }
    );
    return RFIDTag;
};
