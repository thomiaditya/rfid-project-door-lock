"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable("rfidtags", {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            rfid_uid: {
                type: Sequelize.STRING,
                unique: true,
                allowNull: false,
            },
            tag_id: {
                type: Sequelize.STRING,
                allowNull: false,
                unique: true,
            },
            last_seen: {
                type: Sequelize.DATE,
                allowNull: true,
            },
            checked_in: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            createdAt: {
                type: Sequelize.DATE,
                allowNull: false,
            },
            updatedAt: {
                type: Sequelize.DATE,
                allowNull: false,
            },
        });
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable("rfidtags");
    },
};
