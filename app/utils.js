// Import the required libraries
const fs = require('fs');
const speakeasy = require('speakeasy');

// Read the secret from the file
const secret = JSON.parse(fs.readFileSync('totp_secrets/secret.json', 'utf8'));

// Function to verify the OTP
function verifyOTP(token) {
  return speakeasy.totp.verify({
    secret: secret.base32,
    encoding: 'base32',
    token: token,
    window: 1 // Allows a 1-time-step window for verification
  });
}

module.exports = {
    verifyOTP
}

// // Example usage
// const userToken = '123456'; // The OTP to verify
// const isTokenValid = verifyOTP(userToken);

// console.log(`Is the token valid? ${isTokenValid}`);
