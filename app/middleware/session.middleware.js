const logger = require("../logger")

const express = require("express");

// Middleware to check for authentication
const requireAuth = (req, res, next) => {
    console.log(req.session.adminId)
    // Check if user is authenticated
    if (req.session.adminId) {
        // User is authenticated, proceed to next middleware/route handler
        next();
    } else {
        // User is not authenticated, respond with unauthorized error
        res.status(401).json({ success: false, error: 'Unauthorized' });
    }
};

const requireAuthWeb = (req, res, next) => {
    // Check if user is authenticated
    if (req.session.adminId) {
        // User is authenticated, proceed to next middleware/route handler
        next();
    } else {
        // User is not authenticated, respond with unauthorized error
        res.status(404).json({ success: false, error: '404 Not Found!' });
    }
};
module.exports = {
    requireAuth,
    requireAuthWeb
}