const express = require("express");
const ejs = require("ejs");
const session = require("express-session");
const SequelizeStore = require("connect-session-sequelize")(session.Store);
const dotenv = require("dotenv");
const logger = require("./logger");
const requestLogger = require("./middlewares/requestLogger");
const moment = require("moment-timezone");

const apiRouter = require("./routes/api");
const webRouter = require("./routes/web");



// Set the default time zone to Indonesia time (WIB)
moment.tz.setDefault("Asia/Jakarta");

dotenv.config();

const app = express();
app.use(express.static("public"));

// Set the view engine to ejs
app.set("view engine", "ejs");
app.set("views", "./app/views");

// Middleware to parse request bodies
app.use(express.json());

// Sequelize setup
const db = require("./db/models");

// Test the database connection
try {
    db.sequelize.authenticate();
    logger.notice("Connection has been established successfully.");
} catch (error) {
    logger.error("Unable to connect to the database:", error);
}

// Initialize SequelizeStore with Sequelize instance
const sessionStore = new SequelizeStore({
    db: db.sequelize,
    modelKey: "session",
});

app.use(
    session({
        secret: "CK6fH9J4zcifk1mUu94QG2jKUhBZiU",
        resave: true,
        saveUninitialized: false,
        store: sessionStore, // Use SequelizeStore as session store
        cookie: {
            maxAge: 3600000, // Session expiration time (in milliseconds)
        },
    })
);
// Sync session store with database (create session table if it doesn't exist)
sessionStore.sync();

// Routes and middleware
app.use(express.json());
app.use(requestLogger);
app.get("/", (req, res) => {
    return res.status(200).json({ success: true });
});
app.use("/api", apiRouter);
app.use("", webRouter);

// Catch-all route for undefined routes to serve the 404 page
app.use((req, res, next) => {
    res.status(404).json({ success: false, error: "404 Not Found!" });
});

if (process.env.LIST_ROUTES == "true") {
    const expressListRoutes = require("express-list-routes");
    expressListRoutes(app, { prefix: "" });
    expressListRoutes(apiRouter, { prefix: "/api" });
}

const PORT = process.env.PORT || 8000;
app.listen(PORT, "0.0.0.0", () => {
    logger.notice(`Server is running on port ${PORT}`);
});
