// controllers/rfidController.js
const emailTransporter = require("../email");

const models = require("../db/models");
const otpGenerator = require("otp-generator");

exports.listRFIDs = async (req, res) => {
    try {
        // Retrieve all RFID tags from the database
        const rfids = await models.RFIDTag.findAll();
        res.json({ success: true, data: rfids });
    } catch (error) {
        console.error("Error listing RFID tags:", error);
        res.status(500).json({
            success: false,
            error: "Internal server error",
        });
    }
};

exports.createRFID = async (req, res) => {
    try {
        // Create RFID tag in the database
        const rfid = await models.RFIDTag.create({
            rfid_uid: req.body.rfid_uid,
            tag_id: req.body.tag_id,
        });
        res.json({ success: true, rfid });
    } catch (error) {
        console.error("Error creating RFID tag:", error);
        res.status(500).json({
            success: false,
            error: "Internal server error",
        });
    }
};

exports.validate = async (req, res) => {
    try {
        // Retrieve RFID tag from the database
        const rfid = await models.RFIDTag.findOne({
            where: { rfid_uid: req.body.rfid_uid },
            include: [
                {
                    model: models.User,
                    as: "user",
                },
            ],
        });

        if (!rfid) {
            return res.status(404).json({ success: false, error: "RFID not found" });
        }

        // Update rfid `last_seen` property
        // rfid.last_seen = new Date();
        // rfid.checked_in = true;
        // await rfid.save();

        const email = rfid.user.email;

        // Generate OTP
        const otp = otpGenerator.generate(6, {
            digits: true,
            lowerCaseAlphabets: false,
            specialChars: false,
            upperCaseAlphabets: false,
        });
        console.log(otp);

        emailTransporter.sendMail(
            {
                from: `"RFID Project" <${process.env.EMAIL_USERNAME}>`, // sender address
                to: email, // list of receivers
                subject: "One-time Password", // Subject line
                html: `<b>${otp}</b>`, // html body
            },
            (err, resInfo) => {
                console.log("Message sent: %s", resInfo.messageId);
            }
        );

        // Return OTP on success
        res.json(otp);
    } catch (error) {
        console.error("Error validating RFID:", error);
        res.status(500).json({
            success: false,
            error: "Internal server error",
        });
    }
};

exports.checkInConfirm = async (req, res) => {
    try {
        // Retrieve RFID tag from the database
        const rfid = await models.RFIDTag.findOne({
            where: { rfid_uid: req.body.rfid_uid },
            include: [
                {
                    model: models.User,
                    as: "user",
                },
            ],
        });

        if (!rfid) {
            return res.status(404).json({ success: false, error: "RFID not found" });
        }

        // Update rfid `last_seen` property
        rfid.last_seen = new Date();
        rfid.checked_in = true;
        await rfid.save();

        // Save to history log
        const log = await models.Log.create({
            rfid_id: rfid.id,
            is_checked_in: true,
            user_id: rfid.user.id,
        });

        res.status(200).json({
            success: true,
        });
    } catch (error) {
        console.error("Error validating RFID:", error);
        res.status(500).json({
            success: false,
            error: "Internal server error",
        });
    }
};

exports.checkOut = async (req, res) => {
    try {
        // Retrieve RFID tag from the database
        const rfid = await models.RFIDTag.findOne({
            where: { rfid_uid: req.body.rfid_uid, checked_in: true },
            include: [
                {
                    model: models.User,
                    as: "user",
                },
            ],
        });

        if (!rfid) {
            return res.status(404).json({ success: false, error: "RFID not found" });
        }


        res.status(200).json({ success: true });
    } catch (error) {
        console.error("Error validating RFID:", error);
        res.status(500).json({
            success: false,
            error: "Internal server error",
        });
    }
};

exports.checkOutConfirm = async (req, res) => {
    try {
        // Retrieve RFID tag from the database
        const rfid = await models.RFIDTag.findOne({
            where: { rfid_uid: req.body.rfid_uid, checked_in: true },
            include: [
                {
                    model: models.User,
                    as: "user",
                },
            ],
        });

        if (!rfid) {
            return res.status(404).json({ success: false, error: "RFID not found" });
        }

        // Update rfid `last_seen` property
        rfid.checked_in = false;
        await rfid.save();

        // Save to history log
        const log = await models.Log.create({
            rfid_id: rfid.id,
            is_checked_in: false,
            user_id: rfid.user.id,
        });

        // await rfid.save();
        res.status(200).json({ success: true, message: "RFID checked out!" });
    } catch (error) {
        console.error("Error validating RFID:", error);
        res.status(500).json({
            success: false,
            error: "Internal server error",
        });
    }
};

exports.deleteRFID = async (req, res) => {
    try {
        // Retrieve RFID tag from the database
        const rfid = await models.RFIDTag.findOne({
            where: { rfid_uid: req.body.rfid_uid },
            include: [
                {
                    model: models.User,
                    as: "user",
                },
            ],
        });

        if (!rfid) {
            return res.status(404).json({ success: false, error: "RFID not found" });
        }

        // Delete RFID tag from the database
        await rfid.destroy();
        res.status(200).json({ success: true });
    } catch (error) {
        console.error("Error validating RFID:", error);
        res.status(500).json({
            success: false,
            error: "Internal server error",
        });
    }
};

exports.listHistoryLogs = async (req, res) => {
    try {
        // Retrieve all RFID tags from the database
        const logs = await models.Log.findAll({
            include: [
                {
                    model: models.RFIDTag,
                    as: "rfid",
                },
                {
                    model: models.User,
                    as: "user",
                },
            ],
            order: [["createdAt", "DESC"]],
        });
        res.json({ success: true, data: logs });
    } catch (error) {
        console.error("Error listing history logs:", error);
        res.status(500).json({
            success: false,
            error: "Internal server error",
        });
    }
};
