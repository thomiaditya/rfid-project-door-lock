const models = require("../db/models");
const { validationResult } = require("express-validator");

exports.listUsers = async (req, res) => {
    try {
        const users = await models.User.findAll({
            include: [
                {
                    model: models.Status,
                    as: "status",
                },
                {
                    model: models.RFIDTag,
                    as: "rfid",
                },
            ],
        });
        res.json({ success: true, data: users });
    } catch (error) {
        console.error("Error fetching users:", error);
        res.status(500).json({
            success: false,
            error: "Internal server error",
        });
    }
};

exports.createUser = async (req, res) => {
    // Validate request body
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ success: false, errors: errors.array() });
    }

    try {
        const activeStatus = await models.Status.findOne({
            where: {
                name: "ACTIVE",
            },
        });

        req.body.status_id = activeStatus.get("id")

        // Create user in database
        const user = await models.User.create(req.body);

        res.json({ success: true, user });
    } catch (error) {
        console.error("Error creating user:", error);
        res.status(500).json({
            success: false,
            error: "Internal server error",
        });
    }
};

exports.updateUser = async (req, res) => {
    // Validate request body
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ success: false, errors: errors.array() });
    }

    try {
        // Find the user by ID
        const userId = req.params.id;
        const user = await models.User.findByPk(userId);
        if (!user) {
            return res.status(404).json({ success: false, error: 'User not found' });
        }

        // Update user in database
        const updatedUser = await user.update(req.body);
        res.json({ success: true, user: updatedUser });
    } catch (error) {
        console.error('Error updating user:', error);
        res.status(500).json({ success: false, error: error.errors[0].message });
    }
};

exports.deleteUser = async (req, res) => {
    try {
        const userId = req.params.id;
        const user = await models.User.findByPk(userId);
        if (!user) {
            return res.status(404).json({ success: false, error: 'User not found' });
        }
        await user.destroy();
        res.json({ success: true, message: 'User deleted successfully' });
    } catch (error) {
        console.error('Error deleting user:', error);
        res.status(500).json({ success: false, error: 'Internal server error' });
    }
}