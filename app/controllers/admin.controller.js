const logger = require("../logger")

const models = require("../db/models");
const bcrypt = require("bcrypt");
const { validationResult, body } = require("express-validator");

const Admin = models.Admin;

exports.adminLogin = async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({ success: false, errors: errors.array() });
    }

    const { email, password } = req.body;

    try {
        const admin = await Admin.findOne({ where: { email } });
        if (!admin) {
            return res
                .status(401)
                .json({ success: false, error: "Invalid credentials" });
        }

        const passwordMatch = await bcrypt.compare(password, admin.password);
        if (!passwordMatch) {
            return res
                .status(401)
                .json({ success: false, error: "Invalid credentials" });
        }

        req.session.adminId = admin.id;

        res.json({ success: true, message: "Login successful" });
    } catch (error) {
        console.error("Error during admin login:", error);
        res.status(500).json({
            success: false,
            error: "Internal server error",
        });
    }
};

exports.adminLogout = (req, res) => {
    // Destroy session to log out user
    req.session.destroy((err) => {
        if (err) {
            console.error("Error destroying session:", err);
            res.status(500).json({
                success: false,
                error: "Internal server error",
            });
        } else {
            res.json({ success: true, message: "Logout successful" });
        }
    });
};
