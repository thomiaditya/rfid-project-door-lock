const fs = require('fs');

module.exports = {
  development: {
    database: 'database_dev',
    host: 'app/db/db.sqlite',
    dialect: 'sqlite',
    dialectOptions: {
      bigNumberStrings: true
    },
    logging: false,
  },
  test: {
    username: process.env.CI_DB_USERNAME,
    password: process.env.CI_DB_PASSWORD,
    database: process.env.CI_DB_NAME,
    host: 'app/db/db.sqlite',
    dialect: 'sqlite',
    dialectOptions: {
      bigNumberStrings: true
    }
  },
  production: {
    username: process.env.PROD_DB_USERNAME,
    password: process.env.PROD_DB_PASSWORD,
    database: process.env.PROD_DB_NAME,
    host: process.env.PROD_DB_HOSTNAME,
    port: process.env.PROD_DB_PORT,
    dialect: 'sqlite',
  }
};