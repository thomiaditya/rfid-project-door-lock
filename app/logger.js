const winston = require("winston");
const path = require("path");
require("winston-daily-rotate-file");
const appRoot = path.resolve(__dirname, "..");

const { combine, timestamp, label, printf } = winston.format;

const logFormat = printf(({ level, message, label, timestamp }) => {
    return `${timestamp} [${label}] ${level}: ${message}`;
});

const logger = winston.createLogger({
    level: 'info',
    levels: winston.config.syslog.levels,
    format: combine(label({ label: "app" }), timestamp(), logFormat),
    transports: [
        new winston.transports.DailyRotateFile({
            level: "debug",
            filename: "logs/app-%DATE%.log",
            datePattern: "YYYY-MM-DD",
            zippedArchive: true,
            maxSize: "20m",
            maxFiles: "14d",
            dirname: path.join(appRoot, "logs"),
        }),
        new winston.transports.DailyRotateFile({
            level: "error",
            filename: "logs/error-app-%DATE%.log",
            datePattern: "YYYY-MM-DD",
            zippedArchive: true,
            maxSize: "20m",
            maxFiles: "14d",
            dirname: path.join(appRoot, "logs"),
        }),
        new winston.transports.Console({
            level: "notice",
        }),
    ],
});

module.exports = logger;
